//
//  TProc.h
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//
#ifndef TProc_h
#define TProc_h

#include <cstdlib>
#include <ctime>

class TProc{
private:
	bool busyStatus;
	short q2;
public:
	TProc(short feasibility): q2(feasibility){};
	bool isBusy();
	void tact();
	void newJob();
};

#endif /* TProc_h */
