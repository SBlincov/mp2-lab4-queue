//
//  TStack.h
//  Queue
//
//  Created by SBlincov on 22.03.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//
#ifndef TStack_h
#define TStack_h

#include <iostream>
#include "tdataroot.h"
using namespace std;

#define MAX_MEM_SIZE 20

template <class T>
class TStack : public TDataRoot<T>{
protected:
	int Hi; //Last element of queue
	int GetNextIndex(int index) override;
	
public:
	TStack(int Size = MAX_MEM_SIZE) : TDataRoot<T>(Size), Hi(-1) {};
	TStack(const TStack<T>&);
	
	void Put(const T&) override;
	T Get() override
	
	void Print() override;
};

template <class T>
TStack<T>::TStack(const TStack<T> &st) : TDataRoot<T>(st){
	Hi = st.Hi;
}

template <class T>
void TStack<T>::Put(const T &data){
	if (pMem == nullptr)
		throw 1;
	else if (IsFull())
		throw 2;
	else{
		Hi = GetNextIndex(Hi);
		pMem[Hi] = data;
		DataCount++;
	}
}

template <class T>
T TStack<T>::Get(){
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	else if (IsEmpty())
		throw SetRetCode(DataEmpty);
	else{
		DataCount--;
		return pMem[Hi--];
	}
}

template <class T>
void TStack<T>::Print(){
	for (int i = Hi; i > -1; i--)
		cout<<pMem[i]<<endl;
}

template <class T>
int TStack<T>::GetNextIndex(int index){
	return ++index;
}

#endif /* TStack_h */
