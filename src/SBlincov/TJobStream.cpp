//
//  TJobStream.cpp
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright � 2017 Blincov Sergey. All rights reserved.
//

#include "TJobStream.h"
using namespace std;

TJobStream::TJobStream(short q1Temp, short q2Temp) : q1(q1Temp), q2(q2Temp){
	srand(time(0));
	int queueLen = MAX_MEM_SIZE;
	TProc Proc1(feasibility);
	TProc Proc2(feasibility);
	TQueue Queue1(queueLen);
	TQueue Queue2(queueLen);
	idCurrentJob = missedJobs = tacts = missedTacts = 0;
}

void TJobStream::newTact(){
	if(Proc1.isBusy()){
		tacts++;
		Proc1.tact();
	}
	if(Proc2.isBusy() ){
		tacts++;
		Proc2.tact();
	}
	if(!Proc1.isBusy() ){
		if(!Queue1.isFill() ){
			Queue1.get();
			Proc1.newJob();
		} 
		else
			if (!Queue2.isFill() ){
				Queue2.get();
				Proc1.newJob();
			}
			else{
				missedTacts++;
				Proc1.tact();
			}
	}
	if(!Proc2.isBusy() ){
		if(!Queue1.isFill() ){
			Queue1.get();
			Proc2.newJob();
		} 
		else
			if (!Queue2.isFill() ){
				Queue2.get();
				Proc2.newJob();
			}
			else{
				missedTacts++;
				Proc2.tact();
			}
	}
}

void TJobStream::executeJobs(){
	int quantityOfTacts = generateQuantityOfTacts();
	for (int i=0;i<quantityOfTacts;i++){
		if (rand()%100 < q1){
			idCurrentJob++;
			if (!Queue1.isFill())
				Queue1.Put(idCurrentJob);
			else
				if(!Queue2.isFill() )
					Queue2.Put(idCurrentJob);
				else
				missedJobs++;
		}
		newTact();
	}
	while (!Queue1.isFill() || !Queue2.isFill() )
		newTact();
}

void TJobStream::showStatictics(){
	cout << "Quantity tacts: " << tacts + missedTacts <<endl;
	cout << "--Of them was miss" << missedTacts;
	cout << "Quantity jobs: " << idCurrentJob <<endl;
	std::cout << "--Of them was miss: " << missedJobs <<endl;
	cout << "Average quantity tacts per job: " << (double)tacts / (double)(idCurrentJob - missedJobs) <<endl;
}
