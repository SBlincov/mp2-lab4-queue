//
//  TQueue.h
//  Queue
//
//  Created by SBlincov on 03.04.17.
//  Copyright © 2017 Blincov Sergey. All rights reserved.
//
#ifndef TQueue_h
#define TQueue_h

#include "tstack.h"
using namespace std;

template <class T>
class TQueue : public TStack<T>
{
protected:
	int Li; //The first element of queue
	int GetNextIndex(int index) override;
	
public:
	TQueue(int Size = MAX_MEM_SIZE) : TStack<T>(Size), Li(0) {};
	T Get() override;
	void Print() override;
};

template <class T>

int TQueue<T>::GetNextIndex(int index){
	return ++index % MemSize; //MemSize defined in tdataroot
}

template <class T>
T TQueue<T>::Get(){
	T result;
	if (pMem == nullptr)
		throw 1;
	else if (IsEmpty())
		throw 2;
	else{
		result = pMem[Li];
		Li = GetNextIndex(Li);
		DataCount--;
		return result;
	}
}

template <class T>
void TQueue<T>::Print()
{
	for (int i = Li, j = 0; j < DataCount; j++, i = GetNextIndex(i))
		cout<<pMem[i]<<endl;
}

#endif /* TQueue_h */
